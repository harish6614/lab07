/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygon;

/**
 * <p>The RegularPolygon class is a subclass of Polygon and contains attributes and
 * functions to generate computed attributes.</p>
 * 
 * <p>In Euclidean geometry, a regular polygon is a polygon that is equiangular 
 * (all angles are equal in measure) and equilateral (all sides have the same length). 
 * Regular polygons may be either convex or star. In the limit, a sequence of regular 
 * polygons with an increasing number of sides approximates a circle, if the perimeter 
 * or area is fixed, or a regular apeirogon (effectively a straight line), if the edge length is fixed.</p>
 * @author Instructor
 */
public class RegularPolygon extends Polygon{
    
    /**
     * Length of each side of a regular polygon
     */
    private double length;
    
    /**
     * Initializes a newly created {@code Polygon} object using the arguments passed to the constructor.
     * @param noSides Number of Sides of a polygon
     */
    public RegularPolygon(String name,int noSides, double length) {
        super(name, noSides);
        this.length = length;
    }

    public double getLength() {
        return length;
    }
    
    /**
     * {\displaystyle A={\tfrac {1}{4}}ns^{2}\cot \left({\frac {\pi }{n}}\right)}
     * @return 
     */
    public double getArea(){
        double a = (1/4.0) * getNoSides() * Math.pow(length, 2) / Math.tan(Math.PI/length);
        return a;
    }
    
    public double getPerimeter(){
        return getNoSides() * length;
    }
    
    public double getInternalAngle(){
        return (getNoSides()-2) * 180 / length;
    }
    
    public double getInCircleRadius(){
        return length / (2.0 * Math.tan(Math.PI/getNoSides()));
    }

    public double getCircumCircleRadius(){
        return length / (2.0 * Math.sin(Math.PI/getNoSides()));
    }
    
    @Override
    public String toString() {
        return  super.toString()
                +"\n\tLength of side: "+getLength()+"cms"
                +"\n\tInternal angle: "+String.format("%.2f", getInternalAngle())+"\u00B0"
                +"\n\tCircumcircle radius: "+String.format("%.2f", getCircumCircleRadius())+"cms"
                +"\n\tIncircle radius: "+String.format("%.2f", getInCircleRadius())+"cms"
                +"\n\tArea: "+String.format("%.2f", getArea())+"cm\u00B2"
                +"\n\tPerimeter: "+String.format("%.2f", getPerimeter())+"cms";
    }
    
}
