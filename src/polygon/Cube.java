/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygon;

/**
 *
 * @author Harish Bondalapati
 */
public class Cube extends Square{

    public Cube(double length) {
        super("Cube", length);
    }
    
    
    @Override
    public double getArea() {
        return super.getArea()*Solids.CUBE.getNoFaces(); 
    }
    
    public double getInSphereRadius() {
        return getLength() / 2;
    }
    

    public double getCircumSphereRadius() {
        return Math.pow(3, 0.5) * getLength() / 2;
    }
    
    public double getVolume(){
        return Math.pow(getLength(), 3);
    }

    @Override
    public String toString() {
        return super.toString()
                +"\n\tInsphere radius: "+String.format("%.2f", getInSphereRadius())+"cms"
                +"\n\tCircumsphere radius: "+String.format("%.2f", getCircumSphereRadius())+"cms"
                +"\n\tVolume: "+String.format("%.2f", getVolume())+"cm\u00b3"
                ;
    }
    
}
