/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygon;

/**
 *
 * @author Harish Bondalapati
 */
public enum Solids {
    
    TETRAHEDRON(4),
    
    CUBE(6),
    
    BOX(6);
    
    private int noFaces;

    private Solids(int noFaces) {
        this.noFaces = noFaces;
    }

    public int getNoFaces() {
        return noFaces;
    }
    
    
    
}
