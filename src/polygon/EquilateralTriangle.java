/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygon;

/**
 *
 * @author Harish Bondalapati
 */
public class EquilateralTriangle extends RegularPolygon{

    public EquilateralTriangle(double length) {
        super("Equilateral Triangle",3, length);
    }

    public EquilateralTriangle(String name , double length) {
        super(name, 3, length);
    }
    
    public double getHeight(){
        return getLength() * Math.pow(3, 0.5) / 2;
    }
    
}
