/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygon;

/**
 *
 * @author Harish Bondalapati
 */
public class Tetrahedron extends EquilateralTriangle{

    public Tetrahedron(double length) {
        super("Tetrahedron", length);
    }

    @Override
    public double getHeight() {
        return Math.pow(2.0, 0.5) * getLength() / 3.0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArea() {
        return super.getArea() * Solids.TETRAHEDRON.getNoFaces(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public double getVolume(){
        return super.getArea() * getHeight() / 3.0;
    }

    public double getInSphereRadius() {
        return getLength() / Math.pow(24, 0.5); //To change body of generated methods, choose Tools | Templates.
    }
    
    public double getCircumSphereRadius() {
        return getLength() / Math.pow(6, 0.5); //To change body of generated methods, choose Tools | Templates.
    }    

    @Override
    public String toString() {
        return super.toString()
                +"\n\tInsphere radius: "+String.format("%.2f", getInSphereRadius())+"cms"
                +"\n\tCircumsphere radius: "+String.format("%.2f", getCircumSphereRadius())+"cms"
                +"\n\tVolume: "+String.format("%.2f", getVolume())+"cm\u00b3"
                ;
    }
    
}
