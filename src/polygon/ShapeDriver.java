/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygon;

import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Harish Bondalapati
 */
public class ShapeDriver {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        System.out.println("*****************************************");
        Scanner sc = new Scanner(new File("input.txt"));
        ArrayList<Polygon> polygons = new ArrayList<>();
        while (sc.hasNext()) {
            String name = sc.next();
            Polygon p;
            switch (name.toUpperCase()) {
                case "SQUARE":
                    p = new Square(sc.nextDouble());
                    break;
                case "CUBE":
                    p = new Cube(sc.nextDouble());
                    break;
                case "TRIANGLE":
                    p = new EquilateralTriangle(sc.nextDouble());
                    break;
                case "TETRAHEDRON":
                    p = new Tetrahedron(sc.nextDouble());
                    break;
                default:
                    p = new RegularPolygon(
                            name.substring(0, 1).toUpperCase().concat(
                                    name.substring(1).toLowerCase()
                            ),
                            sc.nextInt(), sc.nextDouble());
                    break;
            }
            sc.nextLine();
            polygons.add(p);
        }

        for (Polygon p : polygons) {
            System.out.println("Polygon: " + p);
            System.out.println("*****************************************");
        }
        System.out.println("The polygon with largest area is "
                + getLargePolygon("area", polygons).getName()
                + " with area of "
                + String.format("%.2f", getLargePolygon("area", polygons).getArea())
                + "cm\u00b2"
        );
        System.out.println("The polygon with largest perimeter is "
                + getLargePolygon("perimeter", polygons).getName()
                + " with perimeter of "
                + String.format("%.2f", getLargePolygon("perimeter", polygons).getPerimeter())
                + "cms"
        );
        System.out.println("*****************************************"
                +"\nSurface area to Volume ratio of given solids are:");
        for (Polygon p : polygons) {
            if (p instanceof Cube || p instanceof Tetrahedron) {
                System.out.println(p.getName()
                        + ":\n\tSurface area: " + String.format("%.2f", p.getArea()) + "cm\u00b2"
                );
                if (p instanceof Cube) {
                    System.out.println("\tVolume: "
                            + String.format("%.2f", ((Cube) p).getVolume()) + "cm\u00b3");
                }
                if (p instanceof Tetrahedron) {
                    System.out.println("\tVolume: "
                            + String.format("%.2f", ((Tetrahedron) p).getVolume()) + "cm\u00b3");
                }
            }
        }
    }

    private static Polygon getLargePolygon(String param, ArrayList<Polygon> polygons) {
        Polygon largestPolygon = null;
        switch (param) {
            case "area":
                if (polygons.size() > 1) {
                    largestPolygon = polygons.get(0);
                    for (Polygon p : polygons) {
                        if (largestPolygon.getArea() < p.getArea()) {
                            largestPolygon = p;
                        }
                    }
                }
                break;
            case "perimeter":
                if (polygons.size() > 1) {
                    largestPolygon = polygons.get(0);
                    for (Polygon p : polygons) {
                        if (largestPolygon.getPerimeter() < p.getPerimeter()) {
                            largestPolygon = p;
                        }
                    }
                }
                break;
        }
        return largestPolygon;
    }

}
