/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygon;

/**
 * <p>The Polygon class contains basic attribute of a polygon and functions to generate computed properties like area, perimeter, etc.
 * </p>
 * 
 * <p>In elementary geometry, a polygon is a plane figure that is bounded 
 * by a finite chain of straight line segments closing in a loop to form a closed polygonal chain or circuit. 
 * </p>
 * @author Instructor
 */
public class Polygon {
    /**
     * The private attribute that holds the name of polygon.
     */
    private String name;
    /**
     * The private attribute that holds the number of sides of a polygon.
     */
    private int noSides;
    /**
     * Initializes a newly created {@code Polygon} object using the arguments passed to the constructor.
     * @param noSides Number of Sides of a polygon
     */
    public Polygon(String name,int noSides) {
        this.name = name;
        this.noSides = noSides;
    }
    /**
     * Getter method to access the name of the polygon.
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * Getter method to access the number of sides of the polygon.
     * @return 
     */
    public int getNoSides() {
        return noSides;
    }
    
    /**
     * A method to calculate and return the area of a polygon. This method returns value 0.0 as a double.
     * @return Returns a value 0.0
     */
    public double getArea() {
        return 0.0;
    }

    /**
     * A method to calculate and return the perimeter of a polygon. This method returns value 0.0 as a double.
     * @return Returns a value 0.0
     */
    public double getPerimeter() {
        return 0.0;
    }
    
    /**
     * A method to calculate and return the internal angle of a polygon. This method returns value 0.0 as a double.
     * @return Returns a value 0.0
     */
    public double getInternalAngle() {
        return 0.0;
    }
    
    /**
     * A method to calculate and return the radius of a circle inscribed in the polygon.
     * This method returns value 0.0 as a double.
     * @return Returns a value 0.0
     */    
    public double getInCircleRadius(){
        return 0.0;
    }
    
    
    /**
     * A method to calculate and return the radius of a circle that circumscribes the polygon.
     * This method returns value 0.0 as a double.
     * @return Returns a value 0.0
     */
    public double getCircumCircleRadius(){
        return 0.0;
    }

    @Override
    public String toString() {
        return  getName()
                +"\n\tNumber of sides: "+getNoSides();
    }
    
    
}
